#pragma once

#include <m_maddie.h>
#include <TlHelp32.h>

/*

Remote proc:
Remote process class. Find pattern or retrieve exports. 

*/

class Remoteproc {
private:
	using ProcInfo = std::pair<std::string, DWORD>;
	using Procs = std::vector<ProcInfo>;

	using ModInfo = std::tuple<std::string, std::string, Address, Address>;
	using Mods = std::vector<ModInfo>;

	DWORD m_PID{ };
	HANDLE m_Handle{ };

	ProcInfo m_Self{ };
	Mods m_Modules{ };

public:

	inline Remoteproc( ) { }
	inline ~Remoteproc( ) { }

	// initialize by process name
	inline Remoteproc( const std::string& procName ) {
		auto procs = dumpRunningProcesses( );
		if ( procs.empty( ) ) // what
			return;

		auto search = std::find_if( std::begin( procs ), std::end( procs ), [ & ] ( const ProcInfo& p ) {
			return p.first == procName;
		} );

		if ( search == procs.end( ) )
			return;

		m_Self = *search;

		if ( !m_Self.second )
			return;

		// dump all the modules
		m_Modules = dumpModulesFromProc( m_Self );
		if ( m_Modules.empty( ) )
			return;

		// open with all vm access and thread rights
		m_Handle = OpenProcess( 0x43A, false, m_Self.second );
	}

	// find pattern by reading data with RPM and scanning it normally
	Address scan( const std::string& module, const std::string& pattern ) {
		auto search = std::find_if( std::begin( m_Modules ), std::end( m_Modules ), [ & ] ( const ModInfo& m ) {
			return std::get<0>( m ) == module;
		} );

		if ( search == m_Modules.end( ) )
			return{ };

		auto& mod = *search;
		auto* haystack = new( std::nothrow ) uint8_t[ std::get<3>( mod ) ];
		__stosb( haystack, 0, std::get<3>( mod ) );
		Address haystackCopy = haystack;

		if ( !ReadProcessMemory( m_Handle, std::get<2>( mod ), haystack, std::get<3>( mod ), 0 ) ) {
			delete[ ] haystack;
			return{ };
		}

		Pattern needle( Address( haystack ), std::get<3>( mod ), pattern );
		delete[ ] haystack;

		return Address( needle - Address( haystackCopy ) ) + std::get<2>( mod );
	}

	// find export by loading library and creating module obj on it.
	Address exp( const std::string& module, const std::string& procName ) {
		auto search = std::find_if( std::begin( m_Modules ), std::end( m_Modules ), [ & ] ( const ModInfo& m ) {
			return std::get<0>( m ) == module;
		} );

		if ( search == m_Modules.end( ) )
			return{ };

		auto& modinfo = *search;
		auto hmod = Address( LoadLibraryExA( std::get<1>( modinfo ).c_str( ), NULL, LOAD_LIBRARY_AS_DATAFILE ) );

		auto mod = Module( hmod - 1 );
		for ( auto& exp : mod.m_Eat.getExports( ) ) {
			printf( "%s ... 0x%X\n", std::get<std::string>( exp ).c_str( ), std::get<Address>( exp ) );
		}

		return{ };
	}

	std::string getFullPath( ) {

	}

	template<typename t = Address>t read( Address at ) {
		t ret{ };
		ReadProcessMemory( m_Handle, at, &ret, sizeof( t ), 0 );
		return ret;
	}

	template<typename t>t* copy( Address at, size_t size, t* buf ) {
		ReadProcessMemory( m_Handle, at, buf, size, 0 );
		return buf;
	}

	operator HANDLE( ){ return m_Handle; }
	
	// dump all modules from a process
	static Mods& dumpModulesFromProc( ProcInfo& proc ) {
		static Mods retn{ };
		MODULEENTRY32 me{ };
		HANDLE snapshot{ CreateToolhelp32Snapshot( TH32CS_SNAPMODULE, proc.second ) };
		retn.clear( );

		me.dwSize = sizeof( me );
		do {
			std::wstring path( me.szExePath );
			std::wstring tmp( me.szModule );
			retn.push_back( ModInfo( std::string( tmp.begin( ), tmp.end( ) ), std::string( path.begin( ), path.end( ) ), me.modBaseAddr, me.modBaseSize ) );
		} while ( Module32Next( snapshot, &me ) );

		CloseHandle( snapshot );

		return retn;
	}


	// dump a vector of all running processes info
	static Procs& dumpRunningProcesses( ) {
		static Procs retn{ };
		PROCESSENTRY32 pe{ };
		HANDLE snapshot{ CreateToolhelp32Snapshot( TH32CS_SNAPPROCESS, Null ) };
		retn.clear( );

		pe.dwSize = sizeof( pe );
		do {
			std::wstring tmp( pe.szExeFile );
			retn.push_back( ProcInfo( std::string( tmp.begin( ), tmp.end( ) ), pe.th32ProcessID ) );
		} while ( Process32Next( snapshot, &pe ) );

		CloseHandle( snapshot );

		return retn;
	}
};