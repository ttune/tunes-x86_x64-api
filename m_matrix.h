#include <m_inc.h>

/*

Matrix:
Templated type and size matrix helper.
This might be useless..

*/

template<typename t = float, size_t rows = 3, size_t columns = 4>
class Matrix {
	private:
	using Arr_t = t[ rows ][ columns ];
    using Mat_t = Matrix< t, rows, columns >;
	
	Arr_t m_Data{ };

	public:	
		inline Matrix( ) : m_Data{ 0 }{ }

		inline float& operator[] ( size_t i ) { return m_Data[ i ]; }
		inline float& m( size_t row, size_t place ) { return m_Data[ row ][ place ]; }

		inline float**data( ) { return ( float** )&m_Data; }
};
