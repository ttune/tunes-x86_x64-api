#pragma once

#include "m_inc.h"

using namespace std::chrono;

/*

Timer:
Simple timer class.

*/

template<typename t = milliseconds>
class Timer
{
private:
	time_point<high_resolution_clock> m_start; 	
public:
	inline Timer( ) :
		m_start( high_resolution_clock::now() )
	{ 
		
	}
	~Timer( ) { }
	
	inline auto diff( ){
		return duration_cast<t>( high_resolution_clock::now( ) - m_start ).count( );
	}
	inline void reset( ){
		m_start = high_resolution_clock::now();
	}
	
};