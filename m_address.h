#pragma once

#include "m_inc.h"


/*

Address:
Pointer helper class. 

*/
class Address
{
public:
	Address( ) { };
	~Address( ) { };

	Address( uintptr_t a ) { m_Ptr = a; }
	Address( void* a ) { m_Ptr = ( uintptr_t )a; }

	// Dereference by amt
	uintptr_t get( int dereferences = 0 ) { 
		uintptr_t temp = m_Ptr;
		
		while ( dereferences-- && temp )
			temp = *( uintptr_t* ) temp;
		
		return temp;
	}

	template<typename t = Address>void set( t val ) {
		*( t* ) m_Ptr = val;
	}

	template<typename t = Address>t get( int dereferences = 0 ) {
		return ( t ) get( dereferences );
	}

	template<typename t = Address>t to( ) {
		return ( t ) m_Ptr;
	}

	template<typename t = Address>t as( ) {
		return *( t* ) m_Ptr;
	}

	template<typename t = Address>t at( int offset ){
		return *( t* )( m_Ptr + offset );
	}
	
	// Displace a 64 bit address by a 32 bit relative displacement
	template<typename t = Address, uintptr_t dispOffset = 4>t disp( uintptr_t offset ) {
		uintptr_t code = this->m_Ptr + offset;
		int32_t displacement = *( int32_t* )code;
		
		return ( t )( ( code + displacement ) + dispOffset );
	}
	
	// Check the protections and state of an address and return whether its safe to r/w/x
	static bool safe( Address ad ) {
#ifdef _M_X64
		static MEMORY_BASIC_INFORMATION64 mbi{ };
#else
		static MEMORY_BASIC_INFORMATION32 mbi{ };
#endif
		if ( !VirtualQuery( ad, ( PMEMORY_BASIC_INFORMATION ) &mbi, sizeof( mbi ) ) )
			return false;
		if ( mbi.State != MEM_COMMIT || mbi.Protect == PAGE_NOACCESS || mbi.Protect & PAGE_GUARD )
			return false;

		return true;
	}

	// Various operators
	bool operator!( ) { return !m_Ptr; }
	bool operator==( uintptr_t a ) { return m_Ptr == a; }
	bool operator!=( uintptr_t a ) { return m_Ptr != a; }

	void operator+=( Address a ) { m_Ptr += a; }
	
	operator uintptr_t( ) { return m_Ptr; }
	operator void*( ) { return ( void* )m_Ptr; }
	
protected:
	uintptr_t m_Ptr{ };
};