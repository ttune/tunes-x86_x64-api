#pragma once

#include <m_inc.h>

// universal color class for renderer interface
class Color
{
public:
	byte c[ 4 ];
	Color( int r = 0, int g = 0, int b = 0, int a = 255 ) {
		c[ 0 ] = ( byte ) r;
		c[ 1 ] = ( byte ) g;
		c[ 2 ] = ( byte ) b;
		c[ 3 ] = ( byte ) a;
	}

	operator uint32_t () {
		return *( uint32_t* )this;
	}
	
	operator DWORD() {
		return *( DWORD* )this;
	}
	
	byte& operator[]( int at ) {	return c[ at ]; }

	byte* get( ) { return c; }
};

// commonly used colors
namespace colors {
	namespace Teams {
		const Color axisVisible( 255, 64, 64, 255 );
		const Color axisInvisible( 255, 128, 128, 255 );
		const Color allieVisible( 64, 64, 255, 255 ); 
		const Color allieInvisible( 128, 128, 255, 255 );
	}
	const Color white( 255, 255, 255, 255 );
	const Color grey( 50, 50, 50, 255 );
	const Color red( 255, 64, 64, 255 );
	const Color green( 64, 255, 64, 255 );
	const Color blue( 64, 64, 255, 255 );
	const Color black( 0, 0, 0, 255 );
	const Color lightRed( 255, 128, 128, 255 );
	const Color lightGreen( 128, 255, 128, 255 );
	const Color lightBlue( 128, 128, 255, 255 );
	const Color turquoise( 26, 188, 156, 255 );
	const Color wisteria( 142, 68, 173, 255 );
	const Color lightWisteria( 190, 144, 212, 255 );
}

/*

Renderer interface:
Use this interface to abstract your own renderers by overriding the virtual methods.
This allows for portablitiy to independent guis and such.

*/

class IRender
{
public:
	// make these fonts in every renderer
	enum Font {
		tahoma12,
		arial12,
		fixedsys16
	};

	enum Align {
		left,
		right,
		center
	};

	// override these with dif methods
	virtual void Filled( int x, int y, int w, int h, Color col ) = 0;
	virtual void WText( int x, int y, Color col, Font font, Align align, wchar_t* buf ) = 0;
	virtual void Line( int x1, int y1, int x2, int y2, Color col ) = 0;
	virtual void DrawSpace( int& w, int& h ) = 0;
	
	// text
	void Text( int x, int y, Color col, Font font, Align align, char* buf, ... ) {
		va_list va_alist;
		char szBuffer[ 1024 ] = { 0 };
	
		va_start( va_alist, buf );
		auto len = vsprintf_s( szBuffer, buf, va_alist );
		va_end( va_alist );
	
		auto strsize = MultiByteToWideChar( CP_UTF8, 0, szBuffer, strlen( szBuffer ) + 1, NULL, 0 );
		auto pszStringWide = new wchar_t[ strsize ];
		MultiByteToWideChar( CP_UTF8, 0, szBuffer, strlen( szBuffer ) + 1, pszStringWide, strsize );
	
		WText( x, y, col, font, align, pszStringWide );
		delete[ ] pszStringWide;
	}
	void Text( int x, int y, Color col, Font font, Align align, wchar_t* buf, ... )
	{
		va_list va_alist;
		wchar_t wszBuffer[ 1024 ] = { 0 };
		va_start( va_alist, buf );
		auto len = vswprintf_s( wszBuffer, buf, va_alist );
		va_end( va_alist );
		WText( x, y, col, font, align, buf );
	}
	
	// prims
	void Outlined( int x, int y, int w, int h, Color col ) {
		Filled( x, y, w, 1, col );
		Filled( x, y, 1, h, col );
		Filled( x + w, y, 1, h + 1, col );
		Filled( x, y + h, w, 1, col );
	}
	void Box( int x, int y, int w, int h, Color col ) {
		Outlined( x, y, w, h, col );
		Outlined( x + 1, y + 1, w - 2, h - 2, colors::black );
		Outlined( x - 1, y - 1, w + 2, h + 2, colors::black );
	}
};