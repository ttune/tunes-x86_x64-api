#pragma once

#include "m_address.h"

/*

Hash:
Hash type using djb2 algorithm

*/

class Hash
{
public:
	Hash( uint32_t a ) { m_Data = a; }

	Hash( ) {
		m_Data = 0;
	}

	Hash( const char* str ){
		m_Data = djb2( str );
	}
	
	Hash( const std::string& str ) {
		m_Data = djb2( str.c_str( ) );
	}

	// use djb2 algorithm to get data
	static uint32_t djb2( const char* str ) {
		int32_t c{ };
		uint32_t hash = 5381;

		while ( c = *str++ )
			hash = ( ( hash << 5 ) + hash ) + c; 

		return hash;
	}

	bool operator==( Hash a ){ return a.m_Data == m_Data; }
	bool operator!=( Hash a ) { return a.m_Data != m_Data; }
	
	static Hash make( const std::string& str ) {
		return Hash( str );
	}

	uint32_t m_Data{ };
};