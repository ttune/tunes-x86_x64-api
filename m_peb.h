#pragma once

#include "m_module.h"

/*

Peb:
Highest tier process class. Contains all modules and therefore all exports/imports.

*/

class Peb
{
protected:
	PEB* m_Peb{ };

	std::vector<Module> m_Modules;
public:
	Peb( ) {
#ifdef _M_X64
		m_Peb = ( PEB* ) __readgsqword( 0x60 );
#else
		m_Peb = ( PEB* ) __readfsdword( 0x30 );
#endif 
		Make( );
	};
	~Peb( ) { } ;

	// walk each module and construct it, push back to vector
	bool Make( ) {
		PLIST_ENTRY curEntry{ };
		LDR_DATA_TABLE_ENTRY* cur{ };

		curEntry = m_Peb->Ldr->InLoadOrderModuleList.Flink;

		while ( curEntry != &m_Peb->Ldr->InLoadOrderModuleList && curEntry != nullptr ) {
			cur = CONTAINING_RECORD( curEntry, LDR_DATA_TABLE_ENTRY, InLoadOrderLinks );

			std::wstring wideNameStr( cur->BaseDllName.Buffer );
			std::string nameStr( wideNameStr.begin( ), wideNameStr.end( ) );

#if Debug
#ifdef _M_X64
			printf( "[Peb] %s ... 0x%llX\n", nameStr.c_str( ), cur->DllBase );
#else
			printf( "[Peb] %s ... 0x%X\n", nameStr.c_str( ), cur->DllBase );

#endif  
#endif // Debug

			m_Modules.push_back( Module( nameStr, cur->DllBase, cur->SizeOfImage, Hash( nameStr ) ) );

			curEntry = curEntry->Flink;
		}

		return !m_Modules.empty( );
	}

	std::vector<Module>& modules( ){
		return m_Modules;
	}
	
	Module& operator[] ( uintptr_t mod ) {
		return m_Modules[ mod ];
	}

	uintptr_t base( const std::string& modName ) {
		for ( auto& m : m_Modules ) {
			if ( modName == m.m_Name )
				return m.to<uintptr_t>( );
		}
		return 0;
	}
	uintptr_t size( const std::string& modName ) {
		for ( auto& m : m_Modules ) {
			if ( modName == m.m_Name )
				return m.m_nImageSize;
		}
		return 0;
	}

	// get module by hash
	Module& get( Hash modHash ) {
		for ( auto& m : m_Modules ) {
			if ( modHash.m_Data == m.m_Hash.m_Data )
				return m;
		}
		return m_Modules.front( );
	}
	
	static std::shared_ptr<Peb>& inst(){
		static std::shared_ptr<Peb> singleton{};
		if ( !singleton.get() ) 
			singleton = std::make_shared<Peb>();
			
		return singleton;	
	}
};