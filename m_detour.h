#pragma once

#include "m_address.h"

/*

Detour:
x86/x64 compat. 
Supply a function definition and detour at a specified address.
Save the original function using the template.

*/
template<typename fn>
class Detour
{
public:
	Detour( ) { }
	~Detour( ) {
	}

	Detour( Address hook, Address frame ) : m_Hook( hook ) {
		detour( hook, frame );
	}

	// This uses MS Detours 3.0 Premium. Pirate these or something.
	void detour( Address hook, Address frame ) {
		m_Hook = hook;
		
		// Safely call every detour method required to detour.
		if ( DetourTransactionBegin( ) )
			return;

		if ( DetourUpdateThread( GetCurrentThread( ) ) )
			return;

		if ( DetourAttachEx( ( void** )&m_Hook, frame, &m_pTrampoline, 0, 0 ) )
			return;

		m_Fn = ( fn )m_pTrampoline;
		
		if ( DetourTransactionCommit( ) )
			return;		
		
	}

	fn get( ) { return m_Fn; }
	
	uintptr_t ptr( ) { return ( uintptr_t )m_Fn; }
	
protected:
	fn m_Fn = 0;
	Address m_Hook{ };
	DETOUR_TRAMPOLINE* m_pTrampoline{ };
};