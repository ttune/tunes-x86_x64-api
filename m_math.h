#pragma once

#include <m_matrix.h>

/*

Math:
Various helpers and source engine helpers.

*/

namespace Math {
	const float pi	= 3.141592653589793f;	// pi
	const float pi2	= 6.283185307179586f;	// pi * 2
	const float pid2	= 1.570796326794897f;	// pi / 2
	const float pi1d2	= 0.159154943091895f;	// 0.5 / pi
	
	inline float toRad( float x ) { return x * ( pi / 180.0f ); } // to radians.	
	inline float toDeg( float x ) { return x * ( 180.0f / pi ); } // to degrees.
		
	inline int timeToTicks( const double time, float tickInterval = 0.015625f ) { return ( int )( 0.5f + time / tickInterval ); }	
	inline float ticksToTime( const int ticks, float tickInterval = 0.015625f ) { return tickInterval * ticks; }	
		
	namespace trig {		
		inline void sinCos( float deg, float &outSin, float &outCos ) {
			outSin = sin( deg );
			outCos = cos( deg );
		}		
	};
};