#pragma once

#include "m_timer.h"
#include "m_xor.h"

/*

Console:
A simple winapi debug console class. Used when code is injected into process.

*/

class Console
{
public:
	using marquee = std::vector<std::string>;
private:
	HANDLE m_outHandle{},
		m_inHandle{};

	FILE* m_conout{},
		* m_conin{};

	std::string m_name{};
	
	marquee m_marquee{};
	bool m_inMarquee{};
public:
	inline Console( ) { }

	// Make a console
	inline Console( const std::string& name ) :
		m_name( name )
	{ 
		// Try to allocate
		if ( !AllocConsole( ) )
			return;
		
		// / Attempt to redirect output
		if ( freopen_s( &m_conout, STR( "CONOUT$" ), STR( "w" ), stdout ) )
			return;
		
		// same for input
		if ( freopen_s( &m_conin, STR( "CONIN$" ), STR( "r" ), stdin ) )
			return;
		
		// try set title
		if ( !SetConsoleTitleA( m_name.c_str( ) ) )
			return;
		
		// try to get i/o handles
		m_outHandle = GetStdHandle( STD_OUTPUT_HANDLE );
		if ( m_outHandle == INVALID_HANDLE_VALUE )
			return;
		
		m_inHandle = GetStdHandle( STD_INPUT_HANDLE );
		if ( m_inHandle == INVALID_HANDLE_VALUE )
			return;
		
		// set text color to Cyan
		if ( !SetConsoleTextAttribute( m_outHandle, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_INTENSITY ) )
			return;
	}
	~Console( ) {
		// something?
	}
	
	inline void setTitle( const std::string& title ){ 
		m_name = title;
		m_inMarquee = false;
		SetConsoleTitleA( m_name.c_str( ) );
	}
	
	inline void setMarquee( const marquee& mquee ){
		m_marquee = mquee;
		m_inMarquee = true;
	}
	
	// run in a loop for marquee to display
	inline void run( ){
		if ( !m_inMarquee )
			return;
		
		static Timer<std::chrono::milliseconds> time;
		static size_t iter{ };

		if ( iter >= m_marquee.size( ) )
			iter = 0;

		if ( time.diff( ) > 50 ) { // marquee changes title every 50ms
			SetConsoleTitleA( m_marquee[ iter++ ].c_str( ) );
			time.reset( );
		}
	}

	static Console& inst( const std::string& name = "" ){
		static Console* singleton{};
		if ( singleton == nullptr )
			singleton = new Console( name );
		
		return *singleton;
	}
};