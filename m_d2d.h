#pragma once

#include <m_irenderer.h>

#include <d2d1.h>
#include <DWrite.h>

#pragma comment(lib, "DWrite.lib")
#pragma comment(lib, "d2d1")

/*

D2D Renderer:
An abstracted direct2d renderer. 
It runs off of callbacks which are pushed on to it.

*/

class Render : public IRender
{
public:
	struct Fontdata_t {
		Fontdata_t( std::wstring name, int size = 12 ) : m_nSize( size ), m_Name( name ) { }
		int m_nSize = 12;
		std::wstring m_Name{ };
		IDWriteTextFormat* m_pTextFormat{ };
		IDWriteTextLayout* m_pTextLayout{ };
	};

	Render( ) { };
	~Render( ) { 
		// safely release every d2d obj.
		SAFE_RELEASE( m_pFactory );
		SAFE_RELEASE( m_pRenderTarget );
		SAFE_RELEASE( m_pBrush );
		SAFE_RELEASE( m_pDWriteFactory );
		SAFE_RELEASE( m_pGeometrySink );
		SAFE_RELEASE( m_pPathGeometry );
	};

	Render operator() ( HWND hwnd ) {
		return Render( hwnd );
	}

	Render( HWND hwnd ) : m_hWnd( hwnd ) {
		RECT tmp{ };

		// Try to create a d2d factory
		if ( D2D1CreateFactory( D2D1_FACTORY_TYPE_SINGLE_THREADED, &m_pFactory ) != S_OK )
			return;

		// Get our window rect	
		GetClientRect( m_hWnd, &tmp );
	
		// Create Render target
		m_pFactory->CreateHwndRenderTarget (
			D2D1::RenderTargetProperties( D2D1_RENDER_TARGET_TYPE_DEFAULT, D2D1::PixelFormat( DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED ) ),
			D2D1::HwndRenderTargetProperties( m_hWnd, D2D1::SizeU( tmp.right, tmp.bottom ), /*D2D1_PRESENT_OPTIONS_IMMEDIATELY*/D2D1_PRESENT_OPTIONS_NONE ),
			&m_pRenderTarget 
		);

		// save our width and height
		m_nWidth = tmp.right - tmp.left;
		m_nHeight = tmp.bottom - tmp.top;

		// Create various d2d stuff
		if ( m_pFactory->CreatePathGeometry( &m_pPathGeometry ) != S_OK )
			return;
		
		if ( m_pPathGeometry->Open( &m_pGeometrySink ) != S_OK )
			return;

		if ( m_pRenderTarget->CreateSolidColorBrush( D2D1::ColorF( 0 ), &m_pBrush ) != S_OK )
			return;

		DWriteCreateFactory( DWRITE_FACTORY_TYPE_SHARED, __uuidof( m_pDWriteFactory ), ( IUnknown **)( &m_pDWriteFactory ) );

		// remember to have our required fonts for our interface
		m_Fonts.push_back( Fontdata_t( L"Tahoma" ) );
		m_Fonts.push_back( Fontdata_t( L"Arial" ) );
		m_Fonts.push_back( Fontdata_t( L"Fixedsys", 16 ) );
			
		// init them fonts	
		for ( auto& f : m_Fonts ) {
			m_pDWriteFactory->CreateTextFormat(
				f.m_Name.data( ),
				Null,
				DWRITE_FONT_WEIGHT_NORMAL,
				DWRITE_FONT_STYLE_NORMAL,
				DWRITE_FONT_STRETCH_EXPANDED,
				f.m_nSize,
				L"",
				&f.m_pTextFormat
			);
		}

		// makes text look good :)
		m_pRenderTarget->SetTextAntialiasMode( D2D1_TEXT_ANTIALIAS_MODE_ALIASED );
	}
	
	D2D1_COLOR_F mcol( Color col ) {
		return D2D1::ColorF( col[ 0 ] / 255, col[ 1 ] / 255, col[ 2 ] / 255, col[ 3 ] / 255 );
	}

	// overrides
	virtual void DrawSpace( int& w, int& h ) {
		w = m_nWidth;
		h = m_nHeight;
	}
	
	virtual void Filled( int x, int y, int w, int h, Color col ) {
		D2D1_RECT_F tmp = D2D1::RectF( x, y, x + w, y + h );
		m_pBrush->SetColor( mcol( col ) );
		m_pRenderTarget->FillRectangle( tmp, m_pBrush );
	}

	virtual void Line( int x1, int y1, int x2, int y2, Color col ) {
		m_pBrush->SetColor( mcol ( col ) );
		m_pRenderTarget->DrawLine( D2D1::Point2F( x1, y1 ), D2D1::Point2F( x2, y2 ), m_pBrush );
	}

	virtual void WText( int x, int y, Color col, Font font, Align align , wchar_t * buf ) {
		DWRITE_TEXT_METRICS metrics{ };
		IDWriteTextLayout* textLayout{ };
		Fontdata_t& dat = m_Fonts.at( ( size_t )font );

		// get the text layout for text width
		if ( m_pDWriteFactory->CreateTextLayout(
			buf,
			lstrlenW( buf ),
			dat.m_pTextFormat,
			m_nWidth,
			m_nHeight,
			&textLayout
		) != S_OK )
			return;

		// this was so annoying to figure out properly. i still hate it
		textLayout->GetMetrics( &metrics );
		float layoutWidth = metrics.widthIncludingTrailingWhitespace, modWidth = layoutWidth;
		textLayout->Release( );
		
		if ( align == Align::right )
			modWidth *= 2;
		else if ( align == Align::left )
			modWidth = 0;

		D2D1_RECT_F bounds{ x - ( modWidth * .5 ), y, ( x - ( modWidth * .5 ) ) + m_nWidth, y + m_nHeight };

		// outline - there are better ways to do this
		if ( false ) {
			m_pBrush->SetColor( mcol( colors::black ) );
			D2D1_RECT_F shadows[ ] = {
				{ bounds.left - 1, bounds.top, bounds.right, bounds.bottom },
				{ bounds.left - 1, bounds.top - 1, bounds.right, bounds.bottom },
				{ bounds.left - 1, bounds.top + 1, bounds.right, bounds.bottom },
				{ bounds.left + 1, bounds.top, bounds.right, bounds.bottom },
				{ bounds.left + 1, bounds.top - 1, bounds.right, bounds.bottom },
				{ bounds.left + 1, bounds.top + 1, bounds.right, bounds.bottom },
				{ bounds.left, bounds.top + 1, bounds.right, bounds.bottom },
				{ bounds.left, bounds.top - 1, bounds.right, bounds.bottom }
			};
			for each ( D2D1_RECT_F shade in shadows ) {
				m_pRenderTarget->DrawTextW(
					buf,
					lstrlenW( buf ),
					dat.m_pTextFormat,
					shade,
					m_pBrush
				);
			}
		}

		// draw
		m_pBrush->SetColor( mcol( col ) );
		m_pRenderTarget->DrawTextW(
			buf,
			lstrlenW( buf ),
			dat.m_pTextFormat,
			bounds,
			m_pBrush
		);
	}

	void run( ) {	
		try {
			static float frames{ }, fps{ }, last{ };
			static char timestruct[ 16 ] = "hh':'mm':'ss tt";
			GetTimeFormatA( 0, TIME_FORCE24HOURFORMAT, 0, 0, timestruct, 15 );
			
			
			m_pRenderTarget->BeginDraw( );
			m_pRenderTarget->SetTransform( D2D1::Matrix3x2F::Identity( ) );
			m_pRenderTarget->Clear( D2D1::ColorF( 0, 0, 0, 0 ) );

			Text( m_nWidth - 10, 10, colors::white, Font::tahoma12, Align::right, "[%s Fps: %.0f]", timestruct, fps );

			// call any callbacks added
			for ( auto& cb : m_Callbacks )
				cb( );

			++frames;
			if ( ( ( clock( ) * .001f ) - last ) > 1.f ) {
				last = clock( )*.001f;
				fps = frames;
				frames = 0;
			}

			m_pRenderTarget->EndDraw( );
		}
		catch ( std::out_of_range e ) {
			MessageBoxA( 0, e.what( ), e.what( ), 0x00000030L );
			exit( 1 );
		}
	}

	// callback related helpers.
	void operator += ( Callback cb ) {
		push_task( cb );
	}
	void push_task( Callback cb ) {
		m_Callbacks.push_back( cb );
	}
	void add_tasks( std::initializer_list<Callback> l ) {
		m_Callbacks.insert( m_Callbacks.end( ), l.begin( ), l.end( ) );
	}

protected:
	int m_nWidth{ }, m_nHeight{ };
	std::vector<Fontdata_t> m_Fonts{ };
	std::vector<Callback>  m_Callbacks{ };

	HWND m_hWnd{ };

	IDWriteFactory* m_pDWriteFactory{ };
	ID2D1PathGeometry* m_pPathGeometry{ };
	ID2D1GeometrySink* m_pGeometrySink{ };
	ID2D1Factory* m_pFactory{ };
	ID2D1HwndRenderTarget * m_pRenderTarget{ };
	ID2D1SolidColorBrush* m_pBrush{ };
};