#pragma once

#include "m_hash.h"
#include "m_util.h"
#include "m_iat.h"
#include "m_eat.h"
#include "m_xor.h"

/*

Module:
Dynamic Module class. Initialize EAT and IAT members on constructor.

*/

class Module : public Address
{
public:
	enum class static_indexes {
		self = 0,
		ntdll,
		kernel32,
		kernelbase
	};

	Module( ){ }
	Module( static_indexes index ) {
		PEB* peb{ };
		LDR_DATA_TABLE_ENTRY* cur{ };
		PLIST_ENTRY entry{ };

#ifdef _M_X64
		peb = ( PEB* ) __readgsqword( 0x60 );
#else
		peb = ( PEB* ) __readfsdword( 0x30 );
#endif 
		
		if ( index > static_indexes::kernelbase )
			return;

		// choos from first 4 modules. These are always loaded in this order	
		switch ( index ) {
		case static_indexes::self:
			entry = peb->Ldr->InMemoryOrderModuleList.Flink;
			break;
		case static_indexes::ntdll:
			entry = ( peb->Ldr->InMemoryOrderModuleList.Flink[ 0 ].Flink );
			break;
		case static_indexes::kernel32:
			entry = ( peb->Ldr->InMemoryOrderModuleList.Flink[ 0 ].Flink->Flink );
			break;
		case static_indexes::kernelbase:
			entry = ( peb->Ldr->InMemoryOrderModuleList.Flink[ 0 ].Flink->Flink->Flink );
			break;
		}

		if ( !entry )
			return;

		// construct the module. IAT and EAT.
		cur = CONTAINING_RECORD( entry, LDR_DATA_TABLE_ENTRY, InMemoryOrderLinks );

		std::wstring wideNameStr( cur->BaseDllName.Buffer );
		std::string nameStr( wideNameStr.begin( ), wideNameStr.end( ) );

		m_Ptr = cur->DllBase;
		m_Iat = Iat( m_Ptr );
		m_Eat = Eat( m_Ptr );
		m_nImageSize = cur->SizeOfImage;
		m_Name = nameStr;
		m_Hash = nameStr;
	}

	Module( Address baseAddr ) {
		m_Ptr = baseAddr;
		m_Iat = Iat( m_Ptr );
		m_Eat = Eat( m_Ptr );
	}
	
	Module( std::string name, uintptr_t baseAddr, uintptr_t imageSize, Hash hash ) : m_Name( name ), m_nImageSize( imageSize ), m_Hash( hash ) {
		m_Ptr = baseAddr;
		m_Iat = Iat( m_Ptr );
		m_Eat = Eat( m_Ptr );
	};
	
	// try to get export by name
	auto exp( const std::string& exportName ){
		bool found{ };
		auto ret = m_Eat.get( exportName, found );
		if ( !found )
			std::get<Address>( ret ) = Address( ( uintptr_t )0x0 );
			
		return std::get<Address>( ret ) ;
	}

	// try to get import by name
	auto imp( const std::string& importName ){
		bool found{ };
		auto ret = m_Iat.get( importName, found );
		if ( !found )
			std::get<Address>( ret ) = Address( ( uintptr_t )0x0 );

		return std::get<Address>( ret ) ;
	}
	
	// get the process name without .exe
	static std::string getProcNameTruncated( ){
		static Module self( static_indexes::self );
		static std::string trunc{ self.m_Name };
		if( trunc.find( STR( ".exe" ) ) != std::string::npos ) {
			trunc.resize( trunc.length( ) - 4 );
		}
		return trunc;
	}
	
	std::string m_Name{ };
	uintptr_t m_nImageSize{ };
	Hash m_Hash{ };
	Iat m_Iat{ };
	Eat m_Eat{ };
	
};