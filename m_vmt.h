#pragma once

#include "m_address.h"

/* 

Vmt:
Virtual method table class.
Clone a methods table and replace the pointer (optionally RTTI).

*/

class Vmt
{
public:

	Vmt( ) { }

	Vmt( Address ptr, bool cloneTable = false, bool rtti = false ) {				
		clone( ptr, cloneTable, rtti);
	}

	~Vmt( ) {
		// delete[ ] m_Clone.to<Address*>; 
	}
	
	// copy the table and optionally clone it (and rtti)
	inline bool clone( Address ptr, bool clone, bool rtti ){
		#ifdef _M_X64
		MEMORY_BASIC_INFORMATION64 m_pMBI{ };
#else
		MEMORY_BASIC_INFORMATION32 m_pMBI{ };
#endif
		
		Address* vmt = ptr.get<Address*>( 1 );

		// find how many functions are in the table
		while ( true ) {
			if ( !VirtualQuery( vmt[ m_nFunctions ], ( PMEMORY_BASIC_INFORMATION ) &m_pMBI, sizeof( m_pMBI ) ) )
				break;
			if ( m_pMBI.State != MEM_COMMIT || m_pMBI.Protect == PAGE_NOACCESS || m_pMBI.Protect & PAGE_GUARD )
				break;

#if Debug
#ifdef _M_x64
			printf( "[Vmt:%d] 0x%llX\n", m_nFunctions, vmt[ m_nFunctions ] );
#else
			printf( "[Vmt:%d] 0x%X\n", m_nFunctions, vmt[ m_nFunctions ] );
#endif // _M_X64
#endif // Debug
			m_nFunctions++;
		}

#if Debug
		printf( "m_nFunctions: %d\n", m_nFunctions );
#endif // Debug

		m_Obj = ptr;
		m_Vmt = vmt;
		if ( clone ) {
			m_Clone = new ( std::nothrow )Address[ m_nFunctions ];
			std::memcpy( m_Clone, m_Vmt, m_nFunctions * sizeof( Address ) );
			ptr.set<Address>( m_Clone );
			if ( rtti )
				m_Clone.to<void**>( )[ -1 ] = m_Vmt.to<void**>( )[ -1 ];
		}
		
		return true;
	}

	inline void swap( uint16_t idx, void* h_fn ) {
		m_Clone.to<void**>( )[ idx ] = h_fn;
	}
	
	inline void unhook( ){
		m_Obj.set( m_Vmt );
	}

	template<typename t = uintptr_t>
	t to ( ){
		return ( t ) m_Obj;
	}

	template <typename t = uintptr_t>
	t get( uint16_t idx ) {
		return m_Vmt.to<t*>( )[ idx ];
	}

	template <typename fn>
	static fn vfn( void* vmt, uint16_t index )
	{
		return ( fn ) ( ( *( void*** ) vmt )[ index ] );
	}
	
protected:

	Address m_Obj{ };
	Address m_Vmt{ };
	Address m_Clone{ };

	uint16_t m_nFunctions{ };
};