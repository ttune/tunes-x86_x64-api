#pragma once

#include "m_address.h"

#pragma optimize( "", off ) // needed

/*

Stack:
Get EBP on constructor. next method traverses one frame.

*/

class Stack
{
public:
	// use this to make sure we get EBP of the function this is constructed in
	__forceinline Stack( ){
		m_Ptr = ( uintptr_t )_AddressOfReturnAddress( ) - sizeof( uintptr_t );
	}

	__forceinline Stack( void* AddressOfReturnAddress ) {
		m_Ptr = ( uintptr_t )AddressOfReturnAddress - sizeof( uintptr_t );
	}

	template <typename t>
	t to( ) {
		return ( t ) ( m_Ptr );
	}
	template <typename t>
	t get( size_t offset ) {
		return *( t* ) ( m_Ptr + offset );
	}
	template <typename t = Address>
	t at( size_t offset ) {
		return ( t ) ( m_Ptr + offset );
	}

	inline Address& returnAddress( ) {
		return *( Address* ) ( m_Ptr + sizeof( uintptr_t ) );
	}

	// scan the stack 
	inline Address find( std::string pattern ) {
		Pattern scan( m_Ptr - 0x1000, 0x2000, pattern ); // todo: get esp without inline asm
		return scan.to<Address>( );
	}

	// traverse one frame and return.
	inline Stack next( ) {
		 return m_Ptr ? *( Stack* ) m_Ptr : Null;
	 }

protected:
	 uintptr_t m_Ptr{ };
};

#pragma optimize( "", on ) 
