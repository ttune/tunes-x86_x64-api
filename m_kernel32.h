#pragma once

#include <m_module.h>
#include <m_xor.h>

#define winapi ( __stdcall* )

/*

Kernel32 abstraction:
Call Kernel32 functions without them appearing in your imports.
Feel free to add more.

*/

class Kernel32 {
private:
	using rwProcessMemoryFn = BOOL winapi( HANDLE, uintptr_t, uintptr_t, size_t, size_t* );
	using openProcessFn = HANDLE winapi( DWORD, BOOL, DWORD );
	using createRemoteThreadFn = HANDLE winapi( HANDLE, LPSECURITY_ATTRIBUTES, size_t, LPTHREAD_START_ROUTINE, uintptr_t, DWORD, LPDWORD );
	using virtualAllocExFn = uintptr_t winapi( HANDLE, uintptr_t, size_t, DWORD, DWORD );

	Module m_kernel32{ };
public:

	inline Kernel32( ) :
		m_kernel32{ Module::static_indexes::kernel32 }
	{

	}

	uintptr_t VirtualAllocEx( HANDLE in, uintptr_t addr, size_t size, DWORD allocType, DWORD protect ) {
		static virtualAllocExFn fn{ };
		if ( !fn ) fn = m_kernel32.exp( STR( "VirtualAllocEx" ) ).to<virtualAllocExFn>( );
		return fn( in, addr, size, allocType, protect );
	}

	HANDLE CreateRemoteThread( HANDLE in, LPSECURITY_ATTRIBUTES attrib, size_t size, LPTHREAD_START_ROUTINE proc, uintptr_t startAddr, DWORD creationFlags, LPDWORD threadId ) {
		static createRemoteThreadFn fn{ };
		if ( !fn ) fn = m_kernel32.exp( STR( "CreateRemoteThread" ) ).to<createRemoteThreadFn>( );
		return fn( in, attrib, size, proc, startAddr, creationFlags, threadId );
	}

	HANDLE OpenProcess( DWORD access, BOOL inherit, DWORD pid ) {
		static openProcessFn fn{ };
		if ( !fn ) fn = m_kernel32.exp( STR( "OpenProcess" ) ).to<openProcessFn>( );
		return fn( access, inherit, pid );
	}

	BOOL WriteProcessMemory( HANDLE in, uintptr_t at, uintptr_t buf, size_t len, size_t* numBytesRead = nullptr ) {
		static rwProcessMemoryFn fn{ };
		if ( !fn ) fn = m_kernel32.exp( STR( "WriteProcessMemory" ) ).to<rwProcessMemoryFn>( );
		return fn( in, at, buf, len, numBytesRead );
	}

	BOOL ReadProcessMemory( HANDLE in, uintptr_t at, uintptr_t buf, size_t len, size_t* numBytesRead = nullptr ) {
		static rwProcessMemoryFn fn{ };
		if ( !fn ) fn = m_kernel32.exp( STR( "ReadProcessMemory" ) ).to<rwProcessMemoryFn>( );
		return fn( in, at, buf, len, numBytesRead );
	}


	static Kernel32& inst( ) {
		static Kernel32* singleton{ };
		if ( singleton == nullptr )
			singleton = new Kernel32( );

		return *singleton;
	}
};