#pragma once

#include <m_irenderer.h>

#include <d3d9.h>

#pragma comment( lib, "d3d9.lib" )
#pragma comment( lib, "d3dx9.lib" )

struct Vertex_t {
	float x, y, z, rhw;
	uint32_t m_dwCol;
};

class Render : public IRender
{
public:
	struct Fontdata_t {
		Fontdata_t( std::wstring name, int size = 12 ) : m_nSize( size ), m_Name( name ) { }
		int m_nSize = 12;
		std::wstring m_Name{ };
		ID3DXFont* m_pFont{ };
	};

	Render( ) { };
	~Render( ) {
		SAFE_RELEASE( m_pD3D );
	};

	Render operator() ( HWND hwnd ) {
		return Render( hwnd );
	}

	Render( HWND hwnd ) : m_hWnd( hwnd ) {
		D3DPRESENT_PARAMETERS param{ };
		RECT tmp{ };

		// Create our d3d9 instance
		m_pD3D = Direct3DCreate9( D3D_SDK_VERSION );

		GetClientRect( m_hWnd, &tmp );
		// save our window width/height
		m_nWidth = tmp.right - tmp.left;
		m_nHeight = tmp.bottom - tmp.top;

		memset( &param, 0, sizeof( param ) );

		param.Windowed = true;
		param.SwapEffect = D3DSWAPEFFECT_DISCARD;
		param.hDeviceWindow = m_hWnd;
		param.BackBufferFormat = D3DFMT_A8R8G8B8;
		param.BackBufferWidth = m_nWidth;
		param.BackBufferHeight = m_nHeight;
		param.EnableAutoDepthStencil = true;
		param.AutoDepthStencilFormat = D3DFMT_D16;
		param.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE; // comment for vsync
		param.FullScreen_RefreshRateInHz = 0;

		// Create the device
		m_pD3D->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, m_hWnd, D3DCREATE_SOFTWARE_VERTEXPROCESSING, &param, &m_pDevice );

		// Create a line
		D3DXCreateLine( m_pDevice, &m_pLine );

		// Create our required fonts
		m_Fonts.push_back( Fontdata_t( L"Tahoma" ) );
		m_Fonts.push_back( Fontdata_t( L"Arial" ) );
		m_Fonts.push_back( Fontdata_t( L"Fixedsys", 16 ) );

		for ( auto& f : m_Fonts ) {
			D3DXCreateFont( m_pDevice, f.m_nSize, 0, FW_NORMAL, 1, 0, 1, 0, 4, 0 | FF_DONTCARE, f.m_Name.c_str( ), &f.m_pFont );
		}
	}

	// overrides
	virtual void DrawSpace( int& w, int& h ) {
		w = m_nWidth;
		h = m_nHeight;
	}

	virtual void Filled( int x, int y, int w, int h, Color col ) {
		m_pDevice->SetRenderState( D3DRS_ZENABLE, false );

		Vertex_t qV[ 4 ] = {
			{ ( float ) x, ( float ) ( y + h ), 0.0f, 0.0f, mcol( col ) },
			{ ( float ) x, ( float ) y, 0.0f, 0.0f, mcol( col ) },
			{ ( float ) ( x + w ), ( float ) ( y + h ), 0.0f, 0.0f, mcol( col ) },
			{ ( float ) ( x + w ), ( float ) y, 0.0f, 0.0f, mcol( col ) }
		};

		DWORD _FVF;
		m_pDevice->GetFVF( &_FVF );
		m_pDevice->SetFVF( D3DFVF_XYZRHW | D3DFVF_DIFFUSE );
		m_pDevice->SetPixelShader( NULL );
		m_pDevice->SetTexture( 0, NULL );
		m_pDevice->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, 2, qV, sizeof( Vertex_t ) );
		m_pDevice->SetFVF( _FVF );
	}

	virtual void Line( int x1, int y1, int x2, int y2, Color col ) {
		D3DXVECTOR2 linePoints[ 2 ]{ };
		m_pLine->SetWidth( 1 );

		linePoints[ 0 ] = D3DXVECTOR2{ (float)x1, ( float ) y1 };
		linePoints[ 1 ] = D3DXVECTOR2{ ( float ) x2, ( float ) y2 };

		m_pLine->Draw( linePoints, 2, mcol( col ) );
	}

	virtual void WText( int x, int y, Color col, Font font, Align align, wchar_t * buf ) {
		
		Fontdata_t& dat = m_Fonts.at( ( size_t ) font );

		// Calculate text rectangle to determine width
		RECT tmp{ };
		dat.m_pFont->DrawTextW( 0, buf, -1, &tmp, DT_CALCRECT, 0 );
		float modWidth = tmp.right - tmp.left;

		if ( align == Align::right )
			modWidth *= 2;
		else if ( align == Align::left )
			modWidth = 0;

		RECT bounds{ x - ( modWidth * .5 ), y, x + 50, y + 50 };

		// outline - there are better ways to do this
		if ( true ) {
			RECT shadows[ ] = {
				{ bounds.left - 1, bounds.top, bounds.right, bounds.bottom },
				{ bounds.left - 1, bounds.top - 1, bounds.right, bounds.bottom },
				{ bounds.left - 1, bounds.top + 1, bounds.right, bounds.bottom },
				{ bounds.left + 1, bounds.top, bounds.right, bounds.bottom },
				{ bounds.left + 1, bounds.top - 1, bounds.right, bounds.bottom },
				{ bounds.left + 1, bounds.top + 1, bounds.right, bounds.bottom },
				{ bounds.left, bounds.top + 1, bounds.right, bounds.bottom },
				{ bounds.left, bounds.top - 1, bounds.right, bounds.bottom }
			};
			for each ( RECT shade in shadows ) {
				dat.m_pFont->DrawTextW( 0, buf, -1, &shade, DT_NOCLIP, mcol( colors::black ) );			
			}
		}
		dat.m_pFont->DrawTextW( 0, buf, -1, &bounds, DT_NOCLIP, mcol( col ) );	
	}

	D3DCOLOR mcol( Color col ) {
		return D3DCOLOR_ARGB( col[ 3 ], col[ 0 ], col[ 1 ], col[ 2 ] );
	}

	void run( ) {
		try {
			static float frames{ }, fps{ }, last{ };
			static char timestruct[ 16 ] = "hh':'mm':'ss tt";
			GetTimeFormatA( 0, TIME_FORCE24HOURFORMAT, 0, 0, timestruct, 15 );

			m_pDevice->Clear( 0, NULL, D3DCLEAR_TARGET, D3DCOLOR_ARGB( 0, 0, 0, 0 ), 1.0f, 0 );
			m_pDevice->BeginScene( );

			Text( m_nWidth - 10, 10, colors::white, Font::tahoma12, Align::right, "[%s Fps: %.0f]", timestruct, fps );

			for ( auto& cb : m_Callbacks )
				cb( );

			++frames;
			if ( ( ( clock( ) * .001f ) - last ) > 1.f ) {
				last = clock( )*.001f;
				fps = frames;
				frames = 0;
			}

			m_pDevice->EndScene( );
			m_pDevice->Present( 0, 0, 0, 0 );
		}
		catch ( std::out_of_range e ) {
			MessageBoxA( 0, e.what( ), e.what( ), 0x00000030L );
			exit( 1 );
		}
	}

	void operator += ( Callback cb ) {
		push_task( cb );
	}
	void push_task( Callback cb ) {
		m_Callbacks.push_back( cb );
	}
	void add_tasks( std::initializer_list<Callback> l ) {
		m_Callbacks.insert( m_Callbacks.end( ), l.begin( ), l.end( ) );
	}

protected:
	int m_nWidth{ }, m_nHeight{ };
	std::vector<Fontdata_t> m_Fonts{ };
	std::vector<Callback>  m_Callbacks{ };

	HWND m_hWnd{ };

	IDirect3D9* m_pD3D{ };
	IDirect3DDevice9* m_pDevice{ };
	ID3DXLine* m_pLine{ };
};