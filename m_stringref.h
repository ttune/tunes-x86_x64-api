#pragma once

#include <m_pattern.h>

/*

String reference:
Find a reference to a string (usually in code).

*/

class Stringref : public Pattern
{
public:
	// supply the module and the string to find (which reference is optional)
	Stringref( const std::string& mod, const char* str, size_t whichRef = 0 ) {
		Address base = Peb::inst( )->base( mod );
		Address size = Peb::inst( )->size( mod );

		m_pDosHeader = base.to<PIMAGE_DOS_HEADER>( );
		if ( m_pDosHeader->e_magic != IMAGE_DOS_SIGNATURE )
			return;

		m_pNTHeader = Address( ( byte* ) m_pDosHeader + m_pDosHeader->e_lfanew ).to<PIMAGE_NT_HEADERS>( );
		m_pOptHeader = &m_pNTHeader->OptionalHeader;
#ifndef _M_X64
		//base = m_pOptHeader->BaseOfData;
#else		
		// cant find solution here yet
#endif

		Address stringInData = findByString( base, size, str );
		Address stringRef = findByReference( base, size, stringInData );

		while ( whichRef-- )
			stringRef = findByReference( stringRef, size - ( stringRef - base ), stringInData );

		this->m_Ptr = stringRef;
	}

	Stringref( uintptr_t start, uintptr_t range, const char* str, size_t whichRef = 0 ) {
		Address stringInData = findByString( start, range, str );
		Address stringRef = findByReference( start, range, stringInData );

		while ( whichRef-- )
			stringRef = findByReference( stringRef, range - ( stringRef - start ), stringInData );

		this->m_Ptr = stringRef;
	}

protected:
	Address m_pMod{ };

	PIMAGE_DOS_HEADER m_pDosHeader{ };
	PIMAGE_NT_HEADERS m_pNTHeader{ };
	PIMAGE_OPTIONAL_HEADER m_pOptHeader{ };
};