#pragma once

#include <m_inc.h>
#include <cstdint>

#pragma warning( disable: 4996 )
#pragma warning(disable : 4244)
#pragma warning(disable : 4146)
#pragma warning(disable : 4307)

#ifndef _DEBUG
namespace cx {
	namespace err {
		namespace {
			const char* fnv1_runtime_error;
			const char* fnv1a_runtime_error;
		}
	}
	namespace detail {
		namespace fnv {
			constexpr uint64_t fnv1( uint64_t h, const char* s ) {
				return ( *s == 0 ) ? h :
					fnv1( ( h * 1099511628211ull ) ^ static_cast<uint64_t>( *s ), s + 1 );
			}
			constexpr uint64_t fnv1a( uint64_t h, const char* s ) {
				return ( *s == 0 ) ? h :
					fnv1a( ( h ^ static_cast<uint64_t>( *s ) ) * 1099511628211ull, s + 1 );
			}
		}
	}
	constexpr uint64_t fnv1( const char* s ){
		return true ?
			detail::fnv::fnv1( 14695981039346656037ull, s ) :
			throw err::fnv1_runtime_error;
	}
	constexpr uint64_t fnv1a( const char* s ){
		return true ?
			detail::fnv::fnv1a( 14695981039346656037ull, s ) :
			throw err::fnv1a_runtime_error;
	}
}
namespace cx{
  namespace err  {
    namespace    {
      const char* pcg32_runtime_error;
    }
  }

  namespace pcg {
    constexpr uint64_t pcg32_advance(uint64_t s){
      return s * 6364136223846793005ULL + (1442695040888963407ULL | 1);
    }
    constexpr uint64_t pcg32_advance(uint64_t s, int n) {
      return n == 0 ? s : pcg32_advance(pcg32_advance(s), n-1);
    }
    constexpr uint32_t pcg32_xorshift(uint64_t s)  {
      return ((s >> 18u) ^ s) >> 27u;
    }
    constexpr uint32_t pcg32_rot(uint64_t s)  {
      return s >> 59u;
    }
    constexpr uint32_t pcg32_output(uint64_t s) {
      return (pcg32_xorshift(s) >> pcg32_rot(s))
        | (pcg32_xorshift(s) << ((-pcg32_rot(s)) & 31));
    }
  }

  namespace {
    namespace detail_pcg {
      constexpr int BIT_DEPTH = 6;          // max value = 4095
      constexpr int MAX = (1 << BIT_DEPTH); // recursive template depth = 69
      constexpr int BIT_MASK = MAX - 1;

      template <uint64_t S, int H, int L> struct flag1 {
        friend constexpr int adl_flag1(flag1<S, H, L>);
      };
      template <uint64_t S, int H> struct flag2 {
        friend constexpr int adl_flag2(flag2<S, H>);
      };

      template <uint64_t S, int H>struct r1 {
        template <int L, int = adl_flag1(flag1<S, H, L>{})> static constexpr int reader(int, flag1<S, H, L>) {
          return L;
        }
        template <int L> static constexpr int reader(
            float, flag1<S, H, L>, int R = reader(0, flag1<S, H, L-1>{}))
        {
          return R;
        }
        static constexpr int reader(float, flag1<S, H, 0>) {
          return 0;
        }
      };

      template <uint64_t S>
      struct r2
      {
        template <int H, int = adl_flag2(flag2<S, H>{})> static constexpr int reader(int, flag2<S, H>) {
          return H;
        }
        template <int H> static constexpr int reader(
            float, flag2<S, H>, int R = reader(0, flag2<S, H-1>{})) {
          return R;
        }
        static constexpr int reader(float, flag2<S, 0>) {
          return 0;
        }
      };

      template <uint64_t S, int H, int L> struct writelo {
        friend constexpr int adl_flag1(flag1<S, H, L>) {
          return L;
        }
        static constexpr int value = L;
      };
      template <uint64_t S, int H, bool B> struct writehi {
        friend constexpr int adl_flag2(flag2<S, H>)  {
          return H;
        }
        static constexpr int value = H;
      };
      template <uint64_t S, int H> struct writehi<S, H, false> {
        static constexpr int value = H;
      };

      template <uint64_t S, int H, int L> struct writer {
        static constexpr int hi_value =
          writehi<S, H+1, L == MAX>::value;
        static constexpr int lo_value =
          writelo<S, H, (L & BIT_MASK)>::value;
        static constexpr uint32_t value =
          pcg::pcg32_output(pcg::pcg32_advance(S, (H << BIT_DEPTH) + L));
      };
    }
  }

  namespace pcg {
    template <uint64_t S, int N = 1, int H = detail_pcg::r2<S>::reader(0, detail_pcg::flag2<S, detail_pcg::MAX-1>{}), int L = detail_pcg::r1<S, H>::reader(0, detail_pcg::flag1<S, H, detail_pcg::MAX>{})> inline constexpr uint32_t pcg32(
        uint32_t R = detail_pcg::writer<S, H, L + N>::value)
    {
      return true ? R :
        throw err::pcg32_runtime_error;
    }
  }
}

#define cx_pcg32 cx::pcg::pcg32<cx::fnv1(__FILE__ __DATE__ __TIME__) + __LINE__>
namespace cx {
	namespace err {
		namespace {
			const char* strenc_runtime_error;
		}
	}

	namespace detail {
		template <uint64_t S> constexpr char encrypt_at( const char* s, size_t idx ) {
			return s[ idx ] ^
				static_cast<char>( pcg::pcg32_output( pcg::pcg32_advance( S, idx + 1 ) ) >> 24 );
		}
		template <size_t N> struct char_array {
			char data[ N ];
		};

		inline std::string decrypt( uint64_t S, const char* s, size_t n ) {
			std::string ret;
			ret.reserve( n );
			for( size_t i = 0; i < n; ++i )
			{
				S = pcg::pcg32_advance( S );
				ret.push_back( s[ i ] ^ static_cast<char>( pcg::pcg32_output( S ) >> 24 ) );
			}
			return ret;
		}

		template <uint64_t S, size_t ...Is> constexpr char_array<sizeof...( Is )> encrypt( const char *s, std::index_sequence<Is...> ) {
			return{ { encrypt_at<S>( s, Is )... } };
		}
	}
	
	template <uint64_t S, size_t N> class encrypted_string {
	public:
		constexpr encrypted_string( const char( &a )[ N ] )
			: m_enc( detail::encrypt<S>( a, std::make_index_sequence<N - 1>() ) )
		{}

		constexpr size_t size() const { return N - 1; }

		operator std::string() const{
			return detail::decrypt( S, m_enc.data, N - 1 );
		}

	private:
		const detail::char_array<N - 1> m_enc;
	};

	template <uint64_t S, size_t N> constexpr encrypted_string<S, N> make_encrypted_string( const char( &s )[ N ] ){
		return true ? encrypted_string<S, N>( s ) :
			throw err::strenc_runtime_error;
	}
}

#define CX_ENCSTR_RNGSEED uint64_t{cx::fnv1(__FILE__ __DATE__ __TIME__) + __LINE__}
#define cx_make_encrypted_string cx::make_encrypted_string<CX_ENCSTR_RNGSEED>

#define ENC( s ) std::string( cx_make_encrypted_string( s ) )
#define STR( s ) ENC( s ).c_str()
#elif _DEBUG
#define STR( s ) s
#endif


namespace io 
{
	static auto w_printf = [ ] ( const char* fmt, ... ) {
		va_list args;
		va_start( args, fmt );
		vprintf_s( fmt, args );
		va_end( args );
	};

	static auto w_printf_s = [ ] ( const char* fmt, ... ) {
		va_list args;
		va_start( args, fmt );
		vprintf_s( fmt, args );
		va_end( args );
	};

	static auto w_sprintf = [ ] ( char* buf, const char* fmt, ... ) {
		va_list args;
		va_start( args, fmt );
		vsprintf( buf, fmt, args );
		va_end( args );
	};

	static auto w_sprintf_s = [ ] ( char* buf, size_t buf_size, const char* fmt, ... ) {
		va_list args;
		va_start( args, fmt );
		vsprintf_s( buf, buf_size, fmt, args );
		va_end( args );
	};
}