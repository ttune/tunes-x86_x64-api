#pragma once

// all my required includes
// sorry, this is huge.
//
// .. worth it though.
 
#include <Windows.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <memory>
#include <vector>
#include <array>
#include <tuple>
#include <time.h>
#include <iterator>
#include <iomanip>
#include <chrono>
#include <ctime>
#include <thread>
#include <functional>
#include <detours.h>
#include <d3dx9.h>

#pragma comment( lib, "Dwmapi.lib" )
#pragma comment( lib, "detours.lib" )

#define Naked __declspec( naked )
//#define Debug 0
#define Null 0

#define sleep_ns( ns ) std::this_thread::sleep_for( std::chrono::nanoseconds( ns ) )
#define sleep_ms( ms ) std::this_thread::sleep_for( std::chrono::milliseconds( ms ) )
#define sleep_us( us ) std::this_thread::sleep_for( std::chrono::microseconds( us ) )
#define sleep_s( s ) std::this_thread::sleep_for( std::chrono::seconds( s ) )
	
#define _tc __thiscall
#define _sc __stdcall
#define _cd _cdecl

#define SAFE_RELEASE( x ) if ( x ) x->Release( )
#define __create_debug_console( name ) AllocConsole( ); \
	freopen_s( new FILE*, "CONOUT$", "w", stdout ); \
	freopen_s( new FILE*, "CONIN$", "r", stdin ); \
	SetConsoleTitleA( name ); \
	SetConsoleTextAttribute( GetStdHandle( STD_OUTPUT_HANDLE ), FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_INTENSITY )
	
using Callback = std::function<void( void )>;
using Handle = uint32_t;
using opcode = uint8_t;

#ifdef _AMD64_
#define _PTR_MAX_VALUE ((PVOID)0x000F000000000000)
#else
#define _PTR_MAX_VALUE ((PVOID)0xFFE00000)
#endif	
	
//#define pi ( float )3.14159265358979323846f
#define rad(a) a*(( float )3.14159265358979323846f/180.f)
#define deg(a) a*(180.f/( float )3.14159265358979323846f)
#define square( x ) ( x * x )
	
#pragma warning(disable:4477) // type unsafety
#pragma warning(disable:4313)
#pragma warning(disable:4091) 
#pragma warning(disable:4005) 

