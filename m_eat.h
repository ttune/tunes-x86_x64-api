#pragma once

#include "m_address.h"

/*

Eat:
Export address table class. 
Walks the export address table of a module and saves all the exports and their addresses by hash.

*/

class Eat
{
private:
public:
// name of function, Address of function, hashed named of function
using expFunc = std::tuple<std::string, Address, Hash>;

	inline Eat( ) { }
	inline Eat( Address mod ) :
		m_pMod( mod )
	{
		Make( );
	}

	// try to fill our export vector
	bool Make( ) {
		int functions{ };
		PIMAGE_IMPORT_DESCRIPTOR it{ };
		
		// check that this is a valid PE image
		auto dosHeader = m_pMod.to<PIMAGE_DOS_HEADER>( );
		if ( dosHeader->e_magic != IMAGE_DOS_SIGNATURE )
			return false;

		// get the ntheader and export directory
		auto ntHeader = Address( ( byte* ) dosHeader + dosHeader->e_lfanew ).to<PIMAGE_NT_HEADERS>( );
		m_ExpDir = Address( m_pMod + ntHeader->OptionalHeader.DataDirectory[ IMAGE_DIRECTORY_ENTRY_EXPORT ].VirtualAddress ).to<IMAGE_EXPORT_DIRECTORY*>( );

		// get the names arr, ordinal arr and export arr
		auto names = Address( m_pMod + m_ExpDir->AddressOfNames ).to<uintptr_t*>( );
		auto name_ordinals = Address( m_pMod + m_ExpDir->AddressOfNameOrdinals ).to<uint16_t*>( );
		auto exports = Address( m_pMod + m_ExpDir->AddressOfFunctions ).to<uintptr_t*>( );
		
		unsigned int name_index{};

		// skip modules with no functions 
		if ( !m_ExpDir->NumberOfFunctions )
			return false;
		
		for ( name_index = 0; name_index < m_ExpDir->NumberOfNames; name_index++ ){		

			// get each function and name and push it on to our vector
			auto fn = exports[ name_ordinals[ name_index ] ];
			auto name = reinterpret_cast< const char* >( m_pMod + names[ name_index ] );	
#if Debug
#ifdef _M_X64
			printf( "[Eat] %s ... 0x%llX\n", name, fn + m_pMod);
#else				
			printf( "[Eat] %s ... 0x%X\n", name, ( uintptr_t ) fn +m_pMod);
#endif  
#endif // Debug

			m_ExportedFunctions.push_back( expFunc( name, fn + m_pMod, Hash( name ) ) );
		}
		
		return true;
	}

	std::vector<expFunc>& getExports( ){
		return m_ExportedFunctions;
	}
	
	// i made this for safety on returning exports that aren't found
	static expFunc nullExport( ) {
		static expFunc retn{ };
		static bool once{ };
		if ( !once ) {
			once = true;
			retn = expFunc( "", Address( ( uintptr_t ) 0 ), Hash( ( uintptr_t ) 0 ) );
		}
		return retn;
	}

	// try to find an export by its name, return a null export if not found.
	expFunc get( const std::string& name, bool& found ){
		if ( m_ExportedFunctions.empty( ) ) {
			found = false;
			return nullExport( );
		}

		auto search = std::find_if( std::begin( m_ExportedFunctions ), std::end( m_ExportedFunctions ), [ & ] ( const expFunc& exp ) {
			return std::get<std::string>( exp ) == name;
		} );

		if ( search == m_ExportedFunctions.end( ) ) {
			found = false;
			return nullExport( );
		}
		found = true;
		return *search;
	}
	
	
protected:
	Address m_pMod{ };

#ifdef _M_X64
	MEMORY_BASIC_INFORMATION64 m_pMBI{ };
#else
	MEMORY_BASIC_INFORMATION32 m_pMBI{ };
#endif

	std::vector<expFunc> m_ExportedFunctions{ };
	IMAGE_EXPORT_DIRECTORY* m_ExpDir{ };
};