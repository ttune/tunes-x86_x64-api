#pragma once

#include "m_address.h"

/*

Iat:
Import address table class. 
Walks the import address table of a module and saves all the imports and their addresses by hash.

*/

class Iat
{
public:
// name of function, Address of function, hashed named of function
	using impFunc = std::tuple<std::string, Address, Hash>;
private:
	
	PIMAGE_DOS_HEADER m_pDosHeader{ };
	PIMAGE_NT_HEADERS m_pNTHeader{ };
	PIMAGE_IMPORT_DESCRIPTOR m_pImportDescriptor{ };
	Address m_pMod{ };

	
	std::vector<impFunc> m_ImportedFunctions{ };
#ifdef _M_X64
	MEMORY_BASIC_INFORMATION64 m_pMBI{ };
#else
	MEMORY_BASIC_INFORMATION32 m_pMBI{ };
#endif

public:	
	Iat( ) { }
	Iat( Address mod ) {
		m_pMod = mod;
		Make( );
	}

	bool Make( ) {
		int functions{ };
		PIMAGE_IMPORT_DESCRIPTOR it{ };

		// check valid PE signature
		m_pDosHeader = m_pMod.to<PIMAGE_DOS_HEADER>( );
		if ( m_pDosHeader->e_magic != IMAGE_DOS_SIGNATURE )
			return false;

		// get nt header and import descriptor
		m_pNTHeader = Address( ( byte* ) m_pDosHeader + m_pDosHeader->e_lfanew ).to<PIMAGE_NT_HEADERS>( );
		m_pImportDescriptor = Address( ( byte* ) m_pDosHeader + m_pNTHeader->OptionalHeader.DataDirectory[ IMAGE_DIRECTORY_ENTRY_IMPORT ].VirtualAddress ).to<PIMAGE_IMPORT_DESCRIPTOR>( );
	
		// walk each import and check safety then store in vector
		for ( it = m_pImportDescriptor; it->Name != 0; it++ ) {
			for ( ; *( functions + ( void** ) ( it->FirstThunk + m_pMod ) ) != nullptr; functions++ ) {
				
				uintptr_t* addr = ( functions + ( size_t* ) ( it->OriginalFirstThunk + m_pMod ) );
				if ( !VirtualQuery( addr, ( PMEMORY_BASIC_INFORMATION ) &m_pMBI, sizeof( m_pMBI ) ) )
					continue;

				if ( m_pMBI.State != MEM_COMMIT || m_pMBI.Protect == PAGE_NOACCESS || m_pMBI.Protect & PAGE_GUARD )
					continue;

				char* name = ( char* ) ( *addr + m_pMod + 2 );
				void** fn = functions + ( void** ) ( it->FirstThunk + m_pMod );

				if ( !VirtualQuery( name, ( PMEMORY_BASIC_INFORMATION ) &m_pMBI, sizeof( m_pMBI ) ) )
					continue;

				if ( m_pMBI.State != MEM_COMMIT || m_pMBI.Protect == PAGE_NOACCESS || m_pMBI.Protect & PAGE_GUARD )
					continue;
			
#if Debug
#ifdef _M_X64
				printf( "[Iat] %s ... 0x%llX\n", name, fn );
#else				
				printf( "[Iat] %s ... 0x%X\n", name, ( uintptr_t ) fn );
#endif  
#endif // Debug

				m_ImportedFunctions.push_back( std::tuple<std::string, Address, Hash>( name, fn, Hash( name ) ) );
			}
		}

		return true;
	}
	
	auto& getImports( ){
		return m_ImportedFunctions;
	}

	// get imported function by name
	impFunc& get( const std::string& name, bool& found ){
		auto search = std::find_if( std::begin( m_ImportedFunctions ), std::end( m_ImportedFunctions ), [ & ] ( const impFunc& imp ) {
			return std::get<std::string>( imp ) == name;
		} );

		if ( search == m_ImportedFunctions.end( ) ) {
			found = false;
			return m_ImportedFunctions.front( );
		}

		found = true;
		return *search;
	}
	
};