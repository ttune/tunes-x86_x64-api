#pragma once

#include "m_address.h"
#include "m_peb.h"
#include "m_util.h"

#define INRANGE(x,a,b)		(x >= a && x <= b) 
#define bit_from_pattern( x )		(INRANGE( x,'0','9' ) ? ( x - '0' ) : ( ( x & ( ~0x20 ) ) - 'A' + 0xa ))
#define byte_from_pattern( x )		(bit_from_pattern( x [ 0 ] ) << 4 | bit_from_pattern( x [ 1 ] ))

#pragma optimize( "", off ) // needed

/*

Pattern:
Find data, strings, references, patterns.

Credits: learn_more

*/

class Pattern : public Address
{
public:
	Pattern( ) { };
	~Pattern( ) { };

	// attempt to find address by byte pattern
	uintptr_t findByPattern( uintptr_t start, uintptr_t range, const char* pattern ) {
		const uint8_t* pat = ( const uint8_t* ) ( pattern );
		uint8_t* firstMatch{};
		for ( uint8_t* it = ( uint8_t* )( start ); it < ( uint8_t* ) start + range; ++it ) {
			__try {
				if ( *( uint8_t* ) pat == 63 || *it == byte_from_pattern( pat ) ) {
					if ( !*pat )
						return ( uintptr_t )firstMatch;
					if ( !firstMatch )
						firstMatch = it;
					if ( !pat[2] )
						return ( uintptr_t )firstMatch;
					
					pat += ( *( uint16_t* ) pat == 16191 || *( uint8_t* ) pat != 63 ) ? 3 : 2;

					if ( !*pat )
						return ( uintptr_t ) firstMatch;
				}
				else {
					if ( firstMatch != 0 )					
						it = firstMatch;
					
					pat = ( uint8_t* ) ( pattern );
					firstMatch = 0;
				}
			}
			__except ( 1 ) {  };
		}
		return 0;
	}
	
	// find a pointer to data
	template<typename t = uintptr_t>
	uintptr_t findByData( uintptr_t start, uintptr_t range, t* src ){
		MEMORY_BASIC_INFORMATION mbi{};
		__try {
			for ( uintptr_t it = start; it < start + range; it++ ){	
				if ( it == start || !( it % util::getPageSize( ) ) ) {
					if ( !VirtualQuery( ( void* ) it, ( PMEMORY_BASIC_INFORMATION ) &mbi, sizeof( mbi ) ) )
						continue;
				}

				if ( mbi.State != MEM_COMMIT || mbi.Protect == PAGE_NOACCESS || mbi.Protect & PAGE_GUARD )
						continue;
					
				if ( memcmp( ( void* )it, ( void* )src, sizeof( t ) ) != 0 )
						continue;
				else
					return it;
				
			}
		}
		__except( 1 ){ };
		
		return 0;
	}

	// find a reference of an address in code
	uintptr_t findByReference( uintptr_t start, uintptr_t range, uintptr_t search ){
		#ifndef _M_X64
		return findByData( start, range, &search ); // displacement not necessary on x86
		#else
		__try {
			MEMORY_BASIC_INFORMATION mbi{};
			for ( uintptr_t it = start; it < start + range; it++ ){	
				if ( !VirtualQuery( ( void* )it, ( PMEMORY_BASIC_INFORMATION ) &mbi, sizeof( mbi ) ) )
						continue;

				if ( mbi.State != MEM_COMMIT || mbi.Protect == PAGE_NOACCESS || mbi.Protect & PAGE_GUARD )
						continue;
					
				int32_t displacement = *( int32_t* )it; // must displace on x64?
				uintptr_t cmp = ( uintptr_t )( ( it + displacement ) + 4 );
				
				if ( cmp == search )
					return it;
				
			}
		}
		__except( 1 ){ };
		return 0;
		#endif
	}
	
	// find a string in data
	uintptr_t findByString( uintptr_t start, uintptr_t range, const char* str ){
		MEMORY_BASIC_INFORMATION mbi{};
		__try {	
			for ( uintptr_t it = start; it < start + range; it++ ){	
				if ( !VirtualQuery( ( void* )it, ( PMEMORY_BASIC_INFORMATION ) &mbi, sizeof( mbi ) ) )
					continue;

				if ( mbi.State != MEM_COMMIT || mbi.Protect == PAGE_NOACCESS || mbi.Protect & PAGE_GUARD )
					continue;
					
				if ( it == ( uintptr_t ) str )
					continue;
					
				if ( !strcmp( ( const char* )it, str ) )
					return it;
			}
		}
		__except( 1 ){ };
		
		return 0;
	}
	// constructor
	inline Pattern( const std::string& mod, const std::string& pat, bool retry = false, size_t which = 0 ) {
		Address base = Peb::inst( )->base( mod );
		Address size = Peb::inst( )->size( mod );			

		this->m_Ptr = findByPattern( base, size, pat.c_str( ) );

		do  {
			if ( !which )
				continue;

			this->m_Ptr = findByPattern( this->m_Ptr + 0x1, size - ( this->m_Ptr - base ), pat.c_str( ) );
			which--;
		} while ( which > 0 );

		if ( retry ){
			while ( !this->m_Ptr ) {
				this->m_Ptr = findByPattern( base, size, pat.c_str( ) );
			}
		}
	}

	inline Pattern( uintptr_t start, uintptr_t range, const std::string& pattern, bool retry = false, size_t which = 0 ) {
		this->m_Ptr = findByPattern( start, range, pattern.c_str( ) );

		do {
			if ( !which )
				continue;

			this->m_Ptr = findByPattern( this->m_Ptr + 0x1, range - ( this->m_Ptr - start ), pattern.c_str( ) );
			which--;
		} while ( which > 0 );

		if ( retry ){
			while ( !this->m_Ptr ) {
				this->m_Ptr = findByPattern( start, range, pattern.c_str( ) );
			}
		}
	}
};

#pragma optimize( "", on ) // needed