#pragma once

#include <m_inc.h>

#ifdef _M_X64 //x64

#pragma code_seg(push, ".text")
__declspec( allocate( ".text" ) )
const byte buf[ 4 ] = { 0x48, 0x8B, 0x01, 0xC3 };
#pragma code_seg()

using __veh_eh = void*(*)( void* );

static const __veh_eh veh_eh = ( __veh_eh ) &buf;

static inline bool SafePtr( PVOID Ptr )
{
	return ( Ptr >= ( PVOID ) 0x10000 ) && ( Ptr < _PTR_MAX_VALUE ) && veh_eh( Ptr ) != ( void* ) 0xcafebabe;
}

#define __attach_vectored_exception_handler AddVectoredExceptionHandler( 1, veh_fn );
#define __create_vectored_exception_handler				\
long __stdcall veh_fn( _EXCEPTION_POINTERS* e ) { 			\
	if ( e->ContextRecord->Rip != ( uintptr_t ) buf ) 	\
		return EXCEPTION_CONTINUE_SEARCH; 				\
														\
	e->ContextRecord->Rip += 3; 						\
	e->ContextRecord->Rax = 0xcafebabe;					\
	return EXCEPTION_CONTINUE_EXECUTION;				\
}


#else //x86

#endif