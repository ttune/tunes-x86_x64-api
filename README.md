# tune's x86_x64 API (Windows) + d2d/d3d Renderer #

This is the set of classes I use across all my hacks. 
included are two renderers - for d3d and d2d.

### Types ###
* Address
* Hash
* Vector<Type, Size>
* Matrix<Type, Rows, Columns>

### Dynamic Types ###
* Stack
* Timer
* Overlay
* Console
* Pattern
* String Reference

### Hook Types ###
* VMT
* Detour
* IAT
* EAT

### Process Types ###
* PEB
* Module
* Import Table
* Export Table
* Remote Process

### Util ###
* String Encryption
* Kernel32 Abstraction
* D3D/D2D Renderer
* Renderer Interface