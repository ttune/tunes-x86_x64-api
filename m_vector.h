#pragma once

#include <m_math.h>

/*

Vector:
Templated and variadic vector class

credits: ReactiioN, Valve

*/

template<size_t T_Size = 3>
class Vector
{
public:
    using Arr_t = std::array< float, T_Size >;
    using Vec_t = Vector<T_Size>;
 
    Vector( void )
    {
        static_assert( T_Size >= 2, "Vector does require a min. dimension of 2" );
        fill( static_cast< float >( 0 ) );
    }
 
    explicit Vector( const Arr_t& arr ) :
        m_Data( arr )
    {
        static_assert( T_Size >= 2, "Vector does require a min. dimension of 2" );
    }
 
    template < typename... Args >
    Vector( Args... args ) :
        m_Data{ static_cast< float >( std::forward< Args >( args ) )... }
    {
        static_assert( T_Size >= 2, "Vector does require a min. dimension of 2" );
    }
 
	float& operator () ( const size_t index )
    {
        return at( index );
    }
 
    const float& operator () ( const size_t index ) const
    {
        return at( index );
    }
 
	float& operator [] ( const size_t index )
    {
        return at( index );
    }
 
    const float& operator [] ( const size_t index ) const
    {
        return at( index );
    }

	float& at( const size_t index )
    {
        return m_Data.at( index >= T_Size ? T_Size - 1 : index );
    }
 
    const float& at( const size_t index ) const
    {
        return m_Data.at( index >= T_Size ? T_Size - 1 : index );
    }
 
	Vec_t& operator = ( const Vec_t& other )
    {
        for( size_t i = 0; i < T_Size; ++i ) {
            at( i ) = other( i );
        }
        return *this;
    }
 
	Vec_t& operator = ( const Arr_t& other )
    {
        for( size_t i = 0; i < T_Size; ++i ) {
            at( i ) = other.at( i );
        }
        return *this;
    }
 
	Vec_t& operator = ( const float other )
    {
        for( size_t i = 0; i < T_Size; ++i ) {
            at( i ) = other;
        }
        return *this;
    }
 
	Vec_t operator + ( const Vec_t& other )
    {
        Vec_t buf;
        for( size_t i = 0; i < T_Size; ++i ) {
            buf( i ) = at( i ) + other( i );
        }
        return buf;
    }
 
	Vec_t operator + ( const Arr_t& other )
    {
        Vec_t buf;
        for( size_t i = 0; i < T_Size; ++i ) {
            buf( i ) = at( i ) + other.at( i );
        }
        return buf;
    }
 
	Vec_t operator + ( const float& other )
    {
        Vec_t buf;
        for( size_t i = 0; i < T_Size; ++i ) {
            buf( i ) = at( i ) + other;
        }
        return buf;
    }
 
	Vec_t& operator += ( const Vec_t& other )
    {
        for( size_t i = 0; i < T_Size; ++i ) {
            at( i ) += other( i );
        }
        return *this;
    }
 
	Vec_t& operator += ( const Arr_t& other )
    {
        for( size_t i = 0; i < T_Size; ++i ) {
            at( i ) += other.at( i );
        }
        return *this;
    }
 
	Vec_t& operator += ( const float& other )
    {
        for( size_t i = 0; i < T_Size; ++i ) {
            at( i ) += other;
        }
        return *this;
    }
 
	Vec_t operator - ( const Vec_t& other )
    {
        Vec_t buf;
        for( size_t i = 0; i < T_Size; ++i ) {
            buf( i ) = at( i ) - other( i );
        }
        return buf;
    }
 
	Vec_t operator - ( const Arr_t& other )
    {
        Vec_t buf;
        for( size_t i = 0; i < T_Size; ++i ) {
            buf( i ) = at( i ) - other.at( i );
        }
        return buf;
    }
 
	Vec_t operator - ( const float& other )
    {
        Vec_t buf;
        for( size_t i = 0; i < T_Size; ++i ) {
            buf( i ) = at( i ) - other;
        }
        return buf;
    }
 
	Vec_t& operator -= ( const Vec_t& other )
    {
        for( size_t i = 0; i < T_Size; ++i ) {
            at( i ) -= other( i );
        }
        return *this;
    }
 
	Vec_t& operator -= ( const Arr_t& other )
    {
        for( size_t i = 0; i < T_Size; ++i ) {
            at( i ) -= other.at( i );
        }
        return *this;
    }
 
	Vec_t& operator -= ( const float& other )
    {
        for( size_t i = 0; i < T_Size; ++i ) {
            at( i ) -= other;
        }
        return *this;
    }
 
	Vec_t operator * ( const Vec_t& other )
    {
        Vec_t buf;
        for( size_t i = 0; i < T_Size; ++i ) {
            buf( i ) = at( i ) * other( i );
        }
        return buf;
    }
 
	Vec_t operator * ( const Arr_t& other )
    {
        Vec_t buf;
        for( size_t i = 0; i < T_Size; ++i ) {
            buf( i ) = at( i ) * other.at( i );
        }
        return buf;
    }
 

	Vec_t operator * ( const float& other )
    {
        Vec_t buf;
        for( size_t i = 0; i < T_Size; ++i ) {
            buf( i ) = at( i ) * other;
        }
        return buf;
    }
 
	Vec_t& operator *= ( const Vec_t& other )
    {
        for( size_t i = 0; i < T_Size; ++i ) {
            at( i ) *= other( i );
        }
        return *this;
    }
 
	Vec_t& operator *= ( const Arr_t& other )
    {
        for( size_t i = 0; i < T_Size; ++i ) {
            at( i ) *= other.at( i );
        }
        return *this;
    }
 
	Vec_t& operator *= ( const float& other )
    {
        for( size_t i = 0; i < T_Size; ++i ) {
            at( i ) *= other;
        }
        return *this;
    }
 
	Vec_t operator / ( const Vec_t& other )
    {
        Vec_t buf;
        for( size_t i = 0; i < T_Size; ++i ) {
            buf( i ) = at( i ) / other( i );
        }
        return buf;
    }
 
	Vec_t operator / ( const Arr_t& other )
    {
        Vec_t buf;
        for( size_t i = 0; i < T_Size; ++i ) {
            buf( i ) = at( i ) / other.at( i );
        }
        return buf;
    }
 
	Vec_t operator / ( const float& other )
    {
        Vec_t buf;
        for( size_t i = 0; i < T_Size; ++i ) {
            buf( i ) = at( i ) / other;
        }
        return buf;
    }
 
	Vec_t& operator /= ( const Vec_t& other )
    {
        for( size_t i = 0; i < T_Size; ++i ) {
            at( i ) /= other( i );
        }
        return *this;
    }
 
	Vec_t& operator /= ( const Arr_t& other )
    {
        for( size_t i = 0; i < T_Size; ++i ) {
            at( i ) /= other.at( i );
        }
        return *this;
    }
 
    Vec_t& operator /= ( const float& other )
    {
        for( size_t i = 0; i < T_Size; ++i ) {
            at( i ) /= other;
        }
        return *this;
    }
 
    bool operator == ( const Vec_t& other ) const
    {
        for( size_t i = 0; i < T_Size; ++i ) {
            if( at( i ) != other( i ) ) {
                return false;
            }
        }
        return true;
    }

    bool operator == ( const Arr_t& other ) const
    {
        for( size_t i = 0; i < T_Size; ++i ) {
            if( at( i ) != other.at( i ) ) {
                return false;
            }
        }
        return true;
    }
 
    bool operator == ( const float& other ) const
    {
        for( size_t i = 0; i < T_Size; ++i ) {
            if( at( i ) != other ) {
                return false;
            }
        }
        return true;
    }
 
    bool operator != ( const Vec_t& other ) const
    {
        return !( *this == other );
    }

    bool operator != ( const Arr_t& other ) const
    {
        return !( *this == other );
    }

    bool operator != ( const float& other ) const
    {
        return !( *this == other );
    }
 
    static constexpr size_t size( void )
    {
        return T_Size;
    }

    Arr_t values( void ) const
    {
        return m_Data;
    }

    const Arr_t* data( void ) const
    {
        return &m_Data;
    }

    bool empty( void ) const
    {
        return std::all_of( m_Data.begin(), m_Data.end(), []( float value ) {
            return value == static_cast< float >( 0 );
        } );
    }
 
	float get_distance(  Vec_t& other ) const
    {
        return Vec_t( other - ( *this ) ).get_length();
    }
 
	float get_length( void ) const
    {
        return sqrt( get_length_sqr() );
    }

	float get_length_sqr( void ) const
    {
		float buf = static_cast< float >( 0 );
        for( const auto& i : m_Data ) {
            buf += ( i * i );
        }
        return buf;
    }

    void fill( const float value )
    {
        m_Data.fill( value );
    }

    void normalize( void )
    {
        ( *this ) /= get_length();
    }
 

    Vec_t cross_product( const Vec_t& other, const bool normalize_cross_product = false ) const
    {
        static_assert( T_Size == 3, "The cross product can only be calculated for 3 dimensional vectors" );
        auto vec = Vec_t(
            at( 1 ) * other( 2 ) - at( 2 ) * other( 1 ),
            at( 2 ) * other( 0 ) - at( 0 ) * other( 2 ),
            at( 0 ) * other( 1 ) - at( 1 ) * other( 0 )
        );
 
        if( normalize_cross_product ) {
            vec.normalize();
        }
        return vec;
    }
 
	void angleVectors( Vec_t& fwd, Vec_t& right, Vec_t& up ) {
		float sp, sy, sr, cp, cy, cr;

		Math::trig::sinCos( Math::toRad( at( 0 ) ), sp, cp );
		Math::trig::sinCos( Math::toRad( at( 1 ) ), sy, cy );
		Math::trig::sinCos( Math::toRad( at( 2 ) ), sr, cr );

		fwd[ 0 ] = cp * cy;
		fwd[ 1 ] = cp * sy;
		fwd[ 2 ] = -sp;

		right[ 0 ] = -1.0f * sr * sp * cy + -1.0f * cr * -sy;
		right[ 1 ] = -1.0f * sr * sp * sy + -1.0f * cr * cy;
		right[ 2 ] = -1.0f * sr * cp;

		up[ 0 ] = cr * sp * cy + -sr * -sy;
		up[ 1 ] = cr * sp * sy + -sr * cy;
		up[ 2 ] = cr * cp;
	}

	Vec_t crossProduct( float* a, float* b ) {
		Vec_t retn{ };

		retn[ 0 ] = a[ 1 ] * b[ 2 ] - a[ 2 ] * b[ 1 ];
		retn[ 1 ] = a[ 2 ] * b[ 0 ] - a[ 0 ] * b[ 2 ];
		retn[ 2 ] = a[ 0 ] * b[ 1 ] - a[ 1 ] * b[ 0 ];

		return retn;
	}

	void vectorVectors( Vec_t& right, Vec_t& up ) {
		Vec_t tmp{ };

		if ( m_Data[ 0 ] == 0 && m_Data[ 1 ] == 0 ) {
			right[ 0 ] = 0;
			right[ 1 ] = -1;
			right[ 2 ] = 0;
			up[ 0 ] = -m_Data[ 2 ];
			up[ 1 ] = 0;
			up[ 2 ] = 0;
		}
		else {
			tmp[ 0 ] = 0; tmp[ 1 ] = 0; tmp[ 2 ] = 1.0;

			auto* dat = ( float* )m_Data.data( );
			auto* rightdata = ( float* ) right.data( );
			auto* tmpdata = ( float* ) m_Data.data( );

			right = crossProduct( dat, tmpdata );
			//right.normalize( );

			up = crossProduct( rightdata, dat );
			//up.normalize( );
		}
	}

	void vectorAngles( Vec_t& angles ) {
		if ( at( 1 ) == 0.0f && at( 0 ) == 0.0f ) {
			angles[ 0 ] = ( at( 2 ) > 0.0f ) ? 270.0f : 90.0f;
			angles[ 1 ] = 0.0f;
		}
		else {
			float len2d = sqrt( square( at( 0 ) ) + square( at( 1 ) ) );

			angles[ 0 ] = Math::toDeg( atan2f( -at( 2 ), len2d ) );
			angles[ 1 ] = Math::toDeg( atan2f( at( 1 ), at( 0 ) ) );

			if ( angles[ 0 ] < 0.0f ) angles[ 0 ] += 360.0f;
			if ( angles[ 1 ] < 0.0f ) angles[ 1 ] += 360.0f;
		}

		angles[ 2 ] = 0.0f;
	}

	Vec_t rotate( const Vec_t& angle ) 
	{
		Vec_t out{ };
		float mat[ 3 ][ 4 ]{ };

		float sp, sy, sr, cp, cy, cr;

		Math::trig::sinCos( Math::toRad( angle[ 0 ] ), sp, cp );
		Math::trig::sinCos( Math::toRad( angle[ 1 ] ), sy, cy );
		Math::trig::sinCos( Math::toRad( angle[ 2 ] ), sr, cr );

		mat[ 0 ][ 0 ] = cp * cy;
		mat[ 1 ][ 0 ] = cp * sy;
		mat[ 2 ][ 0 ] = -sp;

		float crcy = cr * cy;
		float crsy = cr * sy;
		float srcy = sr * cy;
		float srsy = sr * sy;

		mat[ 0 ][ 1 ] = sp * srcy - crsy;
		mat[ 1 ][ 1 ] = sp * srsy + crcy;
		mat[ 2 ][ 1 ] = sr * cp;

		mat[ 0 ][ 2 ] = sp * crcy + srsy;
		mat[ 1 ][ 2 ] = sp * crsy - srcy;
		mat[ 2 ][ 2 ] = cr * cp;

		mat[ 0 ][ 3 ] = 0.0f;
		mat[ 1 ][ 3 ] = 0.0f;
		mat[ 2 ][ 3 ] = 0.0f;

		out[ 0 ] = this->dot_product( Vec_t( mat[ 0 ][ 0 ], mat[ 0 ][ 1 ], mat[ 0 ][ 2 ] ) );
		out[ 1 ] = this->dot_product( Vec_t( mat[ 1 ][ 0 ], mat[ 1 ][ 1 ], mat[ 1 ][ 2 ] ) );
		out[ 2 ] = this->dot_product( Vec_t( mat[ 2 ][ 0 ], mat[ 2 ][ 1 ], mat[ 2 ][ 2 ] ) );

		return out;
	}

	Vec_t fwd( )
	{
		float sp, sy, cp, cy;

		Math::trig::sinCos( Math::toRad( at( 0 ) ), sp, cp );
		Math::trig::sinCos( Math::toRad( at( 1 ) ), sy, cy );
		
		return Vec_t( cp * cy, cp * sy, -sp ).normalized( );
	}

	Vec_t right( )
	{
		float sp, sy, sr, cp, cy, cr;

		Math::trig::sinCos( Math::toRad( at( 0 ) ), sp, cp );
		Math::trig::sinCos( Math::toRad( at( 1 ) ), sy, cy );
		Math::trig::sinCos( Math::toRad( at( 2 ) ), sr, cr );
		
		return Vec_t(  1 * sr*sp*cy + -1 * cr*-sy, -1 * sr*sp*sy + -1 * cr*cy, -1 * sr*cr ).normalized( );
	}
	
	Vec_t up( )
	{
		float sp, sy, sr, cp, cy, cr;

		Math::trig::sinCos( Math::toRad( at( 0 ) ), sp, cp );
		Math::trig::sinCos( Math::toRad( at( 1 ) ), sy, cy );
		Math::trig::sinCos( Math::toRad( at( 2 ) ), sr, cr );
		
		return Vec_t( cr*sp*cy + -sr*-sy, cr*sp*sy + -sr*cy, cr*cp ).normalized( );
	}
 
	using Matrix34 = Matrix<float, 3, 4>;
	inline Matrix34 matrix( Vec_t origin )
	{
		Matrix34 retn{};
		
		Vec_t vfwd{},
			vright{},
			vup{};
			
		vfwd = fwd( );
		vright = right( );
		vup = up( );
		
		retn.m( 0, 0 ) = vfwd[ 0 ];
		retn.m( 1, 0 ) = vfwd[ 1 ];
		retn.m( 2, 0 ) = vfwd[ 2 ];
		retn.m( 0, 1 ) = vright[ 0 ];
		retn.m( 1, 1 ) = vright[ 1 ];
		retn.m( 2, 1 ) = vright[ 2 ];
		retn.m( 0, 2 ) = vup[ 0 ];
		retn.m( 1, 2 ) = vup[ 1 ];
		retn.m( 2, 2 ) = vup[ 2 ];
		retn.m( 0, 3 ) = origin[ 0 ];
		retn.m( 1, 3 ) = origin[ 1 ];
		retn.m( 2, 3 ) = origin[ 2 ];
		
		return retn;
	}
 
    Vec_t inversed( void ) const
    {
        auto vec = *this;
        const auto one = static_cast< T >( 1 );
        for( auto& i : vec ) {
            i = one / i;
        }
        return vec;
    }

    Vec_t normalized( void ) const
    {
        auto vec = *this;
        vec.normalize();
        return vec;
    }

    template< size_t T2_Size >
	float dot_product( const Vector< T2_Size >& other ) const
    {
        auto dot = static_cast< float >( 0 );
        for( size_t i = 0; i < ( T_Size < T2_Size ? T_Size : T2_Size ); ++i ) {
            dot += at( i ) * other( i );
        }
 
        return dot;
    }
	
	Vec_t transform( Matrix34& mat )
	{	
		static_assert( T_Size == 3, "Vector3 transform can only be calculated for 3 dimensional vectors" );
		Vec_t out{};		
		
		Vec_t in[ 3 ]{
			Vec_t( mat.m( 0, 0 ), mat.m( 0, 1 ), mat.m( 0, 2 ) ),
			Vec_t( mat.m( 1, 0 ), mat.m( 1, 1 ), mat.m( 1, 2 ) ),
			Vec_t( mat.m( 2, 0 ), mat.m( 2, 1 ), mat.m( 2, 2 ) )
		};

		out[0] = this->dot_product( in[ 0 ] ) + mat.m( 0, 3 ); // bf1 matrix
		out[1] = this->dot_product( in[ 1 ] ) + mat.m( 1, 3 ); 
		out[2] = this->dot_product( in[ 2 ] ) + mat.m( 2, 3 ); 
		
		return out;
	}
 
	inline float length2d( ) {
		return std::sqrt( ( v.x * v.x ) + ( v.y * v.y ) );
	}

	Vec_t angle( Vec_t* up = nullptr )
	{
		Vec_t out{ }, left{ };
		float upZ{ };

		// https://github.com/ValveSoftware/source-sdk-2013/blob/master/sp/src/mathlib/mathlib_base.cpp#L1034
		if ( up ) {
			// get left direction vector using cross product.
			left = ( *up ).cross_product( *this );

			// calculate roll.
			upZ = ( left.v.y * v.x ) - ( left.v.x * v.y );

			// out.
			out = Vec_t(
				Math::toDeg( std::atan2( -v.z, length2d( ) ) ),
				Math::toDeg( std::atan2( v.y, v.x ) ),
				Math::toDeg( std::atan2( left.v.z, upZ ) )
			);
		}
		else {
			// out.
			out = Vec_t(
				Math::toDeg( std::atan2( -v.z, length2d( ) ) ),
				Math::toDeg( std::atan2( v.y, v.x ) ),
				0
			);
		}

		return out;
	}

 
	Vec_t vector( ){		
		float p = Math::toRad( this->at( 0 ) );
		float y = Math::toRad( this->at( 1 ) );
		float temp = cos( p );
		
		return Vec_t(
			-temp * -cos( y ),
			sin( y ) * temp,
			-sin( p )
		);
	}
 
    friend std::ostream& operator<<( std::ostream& os, const Vec_t& v )
    {
        for( size_t i = 0; i < T_Size; ++i ) {
            os << v( i );
            if( i + 1 != T_Size ) {
                os << " - ";
            }
        }
        return os;
    }
 
	struct __vec_t {
		float x, y, z;
	};

	union {
		__vec_t v;
		Arr_t m_Data;
	};
};