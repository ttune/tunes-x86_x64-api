#pragma once

#include <m_address.h>
#include <m_util.h>
#include <dwmapi.h>


/*

Overlay:
Create an overlay window on a specified target window.
Uses callbacks to run code. 

*/

class Overlay
{
public:
	Overlay( ) { };
	~Overlay( ) { };

	Overlay( std::wstring window ) : m_WindowName( window.begin( ), window.end( ) ) {
		LONG style{ };

		// try to find the target window
		m_TargetWindow = FindWindow( Null, window.data( ) );
		if ( !m_TargetWindow )
			return;

		// try to get the target windows rect
		if ( !GetWindowRect( m_TargetWindow, &m_Margins ) )
			return;

		// get the windows style
		style = GetWindowLong( m_TargetWindow, GWL_STYLE );

		// fix up position if border is present
		if ( style & WS_BORDER ) {
			m_Margins.top += 25;
			m_Margins.left += 3;	
		}

		// calculate our width and height
		m_nWidth = m_Margins.right - m_Margins.left;
		m_nHeight = m_Margins.bottom - m_Margins.top;

		// fix up position if order is present
		if ( style & WS_BORDER ) {
			m_nWidth -= 3;
			m_nHeight -= 4;
		}

		if ( !make( ) )
			return;
	}

	void position( ) {
		LONG style{ };
		RECT tmp{ };
		// set overlay pos to our target window pos
		SetWindowPos( m_Window, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE );
		GetWindowRect( m_TargetWindow, &tmp );
		style = GetWindowLong( m_TargetWindow, GWL_STYLE );

		// fixup if border present
		if ( style & WS_BORDER ) {
			tmp.top += 25;
			tmp.left += 3;
		}

		// recalc width/height
		m_nWidth = tmp.right - tmp.left;
		m_nHeight = tmp.bottom - tmp.top;

		// fixup pt 2.
		if ( style & WS_BORDER ) {
			m_nWidth -= 3;
			m_nHeight -= 4;
		}

		// adjust window
		MoveWindow( m_Window, tmp.left, tmp.top, m_nWidth, m_nHeight, true );
	}

	int run( ) {
		MSG msg{ };
		// run until quit message
		while ( msg.message != WM_QUIT ) {
			try {
				if ( PeekMessage( &msg, Null, Null, Null, PM_REMOVE ) ) {
					TranslateMessage( &msg );
					DispatchMessage( &msg );
				}
				else {				
					// fixup border
					position( );

					// run all callbacks
					for ( auto& cb : m_Callbacks ) {
						cb( );
					}
				}			
			}
			catch ( std::out_of_range e ) {
				MessageBoxA( 0, e.what( ), e.what( ), 0x00000030L );
				exit( 1 );
			}
		}

		return EXIT_SUCCESS;
	}

	// window maker function
	bool make( ) {
		const wchar_t classname[ ]{ 'm', '_', 'o', 'v', 'e', 'r', 'l', 'a', 'y' }; // yuck!
		const MARGINS margins{ -1 };
		WNDCLASSEX wndclass{ };

		wndclass.cbSize = sizeof( WNDCLASSEX );
		wndclass.hbrBackground = ( HBRUSH ) CreateSolidBrush( RGB( 0, 0, 0 ) );
		wndclass.hInstance = Null;
		wndclass.lpfnWndProc = wproc;

		wndclass.lpszClassName = classname; 
		wndclass.style = CS_HREDRAW | CS_VREDRAW;

		// register proper window class
		RegisterClassEx( &wndclass );
		AdjustWindowRectEx( &m_Margins, WS_OVERLAPPEDWINDOW, false, WS_EX_OVERLAPPEDWINDOW );

		// create the window
		m_Window = CreateWindowEx(
			Null,//WS_EX_LAYERED
			classname,
			L"",
			WS_EX_TOPMOST | WS_POPUP,
			m_Margins.left,
			m_Margins.top,
			m_nWidth,
			m_nHeight,
			Null,
			Null,
			Null,
			Null
		);

		if ( !m_Window )
			return false;

		// adjust window
		SetWindowLong( m_Window, GWL_EXSTYLE, ( int ) GetWindowLong( m_Window, GWL_EXSTYLE ) | WS_EX_LAYERED | WS_EX_TRANSPARENT );
		SetLayeredWindowAttributes( m_Window, RGB( 0, 0, 0 ), 255, ULW_COLORKEY | LWA_ALPHA );
		DwmExtendFrameIntoClientArea( m_Window, &margins );
		ShowWindow( m_Window, 10 );

		return true;
	}

	void operator += ( Callback cb ) {
		push_task( cb );
	}
	void push_task ( Callback cb ) {
		m_Callbacks.push_back( cb );
	}
	void add_tasks( std::initializer_list<Callback> l ) {
		m_Callbacks.insert( m_Callbacks.end( ), l.begin( ), l.end( ) );
	}

	HWND handle( ) { return m_Window; }
	HWND target( ) { return m_TargetWindow; }

protected:
	static LONG_PTR __stdcall wproc( HWND wnd, uint32_t msg, WPARAM wparam, LPARAM lparam ) {
		switch ( msg ) {
		case WM_DESTROY:
			PostQuitMessage( 0 );
			return 0;
		}
		return DefWindowProc( wnd, msg, wparam, lparam );
	}


	std::string m_WindowName{ };
	std::vector<Callback>  m_Callbacks{ };

	int m_nWidth{ }, m_nHeight{ };

	RECT m_Margins{ };
	HWND m_Window{ }, m_TargetWindow{ };
};