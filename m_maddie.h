#pragma once

#include "m_peb.h"
#include "m_address.h"
#include "m_pattern.h"	
#include "m_stack.h"
#include "m_vmt.h"
#include "m_detour.h"
#include "m_xor.h"
#include "m_console.h"
#include "m_stringref.h"
#include "m_kernel32.h"